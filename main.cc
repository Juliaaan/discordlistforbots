/*
 *  discordlistforbots
 *  Copyright (C) 2021  niansa
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <drogon/drogon.h>
#include "config.h"

int main() {
    // Set HTTP listener address and port
    drogon::app().addListener(LISTEN_ADDR, LISTEN_PORT).
            enableSession(std::chrono::minutes(1200)).
            createDbClient(DB_TYPE, DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASSWORD).
            setFileTypes({"css", "png", "ico", "js"});
    // Run HTTP framework, the method will block in the internal event loop
    drogon::app().run();
    return 0;
}
