# Discord list for bots
Open source bot list website, for everyone who wants to drive a Discord bot list. Originally programmed for the list Discordlist For Bots (hence the name)


## Configuration
Create a file named `config.h` in the root then paste and edit the following:

    #define LISTEN_ADDR "0.0.0.0"
    #define LISTEN_PORT 8082
    
    #define DB_TYPE "postgresql"
    #define DB_HOST "192.168.178.39"
    #define DB_PORT 5432
    #define DB_NAME "dfb"
    #define DB_USER "julius"
    #define DB_PASSWORD "6345786435663524675324"
    
    #define OAUTH_URL "https://discord.com/api/oauth2/authorize?client_id=795612465130897420&redirect_uri=http%3A%2F%2Flocalhost:8082%2Fdiscordauth&response_type=code&scope=identify"
    #define CLIENT_ID "795612465130897420"
    #define CLIENT_SECRET "<your secret here>"
    #define REDIRECT_URI "http://localhost:8082/discordauth"
    #define BOT_TOKEN "<your bot token here>"
    #define LOG_WEBHOOK "824208240333291561/eI39bQGclrizr0OzRGrB_W7QI3gfY7tlYpWv8RkkDVzIOVFRDTjgkuavE7YfkIzDcGsy"
    
    #define MODERATORS {609486822715818000, 703944517048598568, 705557092802625576, 664869643567235097, 775017772358434817}
    
    #define COMMUNITY "https://discord.gg/42vDtZxZSt"
    
    #define SHORT_NAME "DFB"
    #define LONG_NAME "Discordlist for Bots"
    #define DESCRIPTION "Find a lot of bots that will be useful to your server"
    
    #define IMPRINT "<your imprint here>"

(Original config.h, no longer in use)

## Adding custom assets
You may add custom assets to the `customstatic` directory and cmake will copy these on next run.

